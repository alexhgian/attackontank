package com.aa.attackontank;

import static com.badlogic.gdx.scenes.scene2d.actions.Actions.moveTo;

import com.badlogic.gdx.graphics.g2d.SpriteBatch;
import com.badlogic.gdx.math.Rectangle;
import com.badlogic.gdx.scenes.scene2d.Actor;
import com.badlogic.gdx.utils.Array;

public class Buildings extends Actor{

	Array<Rectangle> buildings;
	
	
private Rectangle bounds = new Rectangle();
	
	public Buildings(float x, float y) {
		setWidth(85);
		setHeight(85);
		setPosition(x, y - getHeight()/2);

		
		//spawning speed
		addAction(moveTo(-getWidth(), getY(), 3));
		//the third parameter was previously  MathUtils.random(4.0f, 6.0f)
	}
	
	@Override
	public void act(float delta){
		super.act(delta);
		updateBounds();
	}
	
	private void updateBounds() {
		bounds.set(getX(), getY(), getWidth(), getHeight());
	}
	

	public Rectangle getBounds() {
		return bounds;
	}

	@Override
	public void draw(SpriteBatch batch, float parentAlpha) {
		batch.setColor(getColor().r, getColor().g, getColor().b, getColor().a);		
		batch.draw(Assets.building, getX(), getY(), getWidth()/2, getHeight()/2, getWidth(), getHeight(), 1, 1, getRotation());
	}


}
