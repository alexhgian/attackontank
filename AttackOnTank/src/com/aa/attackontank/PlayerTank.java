package com.aa.attackontank;

import com.badlogic.gdx.graphics.Color;
import com.badlogic.gdx.graphics.g2d.SpriteBatch;
import com.badlogic.gdx.math.Rectangle;
import com.badlogic.gdx.scenes.scene2d.Actor;

public class PlayerTank extends Actor{

		private TankGame trafficGame;
		private Rectangle bounds = new Rectangle();
		private int lane;
		
		public PlayerTank(TankGame trafficGame) {
			this.trafficGame = trafficGame;
			setWidth(64);
			setHeight(64);
			lane = 1;
			setPosition(100, 25);
			setColor(Color.YELLOW);
		}
		
		@Override
		public void act(float delta){
			super.act(delta);
			updateBounds();
		}

		@Override
		public void draw(SpriteBatch batch, float parentAlpha) {
			batch.setColor(getColor().r, getColor().g, getColor().b, getColor().a);		
			batch.draw(Assets.tank, getX(), getY(), getWidth()/2, getHeight()/2, getWidth(), getHeight(), 1, 1, getRotation());
		}
		
		private void updateBounds() {
			bounds.set(getX(), getY(), getWidth(), getHeight());
			
		}		
		
		public void moveTank(float x, float y){
			int speed = 5;
			x = getX() + x * speed;
			y = getY() + y * speed;
			
			setX(x);
			setY(y);
			
			if(getX() < 0){
				setX(0);
			} else if (getX() > 800-getWidth()){
				setX(800-getWidth());
			}
			
			if (getY() < 0)
			{
				setY(0);
			}else if( getY() > 125)
			{			
				setY(125);
			}		
		
		}


		public Rectangle getBounds() {
			return bounds;
		}

}
