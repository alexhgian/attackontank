package com.aa.attackontank;

import com.badlogic.gdx.Gdx;
import com.badlogic.gdx.graphics.g2d.TextureAtlas;
import com.badlogic.gdx.graphics.g2d.TextureRegion;

public class Assets {
	
	
	public static TextureAtlas atlas, atlasFB, atlasUI;
	public static TextureRegion tank;
	public static TextureRegion backGround;
	public static TextureRegion building;
	public static TextureRegion bullet;
	public static TextureRegion enemyTank;
	public static TextureRegion enemyBullet;
	

	public static void load() {
		atlas = new TextureAtlas(Gdx.files.internal("GameAssets.txt"));
		atlasFB = new TextureAtlas(Gdx.files.internal("ui/FireButtons.txt"));
		atlasUI = new TextureAtlas(Gdx.files.internal("ui/uiskin.atlas"));
		
		tank = atlas.findRegion("myTank");
		backGround = atlas.findRegion("BG");
		building = atlas.findRegion("building2");
		bullet = atlas.findRegion("bullet");	
		enemyTank = atlas.findRegion("enemyTank");
		enemyBullet = atlas.findRegion("enemyBullet");
		
	}

	public static void dispose() {
		atlas.dispose();
		atlasFB.dispose();
		atlasUI.dispose();
	}

}
