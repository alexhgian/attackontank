package com.aa.attackontank;

import java.util.Iterator;

import com.aa.attackontank.screens.BackGround;
import com.aa.attackontank.ui.MyTouchPad;
import com.aa.attackontank.ui.Styles;
import com.badlogic.gdx.graphics.Color;
import com.badlogic.gdx.graphics.g2d.SpriteBatch;
import com.badlogic.gdx.math.MathUtils;
import com.badlogic.gdx.scenes.scene2d.InputEvent;
import com.badlogic.gdx.scenes.scene2d.InputListener;
import com.badlogic.gdx.scenes.scene2d.ui.Table;
import com.badlogic.gdx.scenes.scene2d.ui.TextButton;
import com.badlogic.gdx.scenes.scene2d.ui.Touchpad;
import com.badlogic.gdx.utils.Array;
import com.badlogic.gdx.utils.TimeUtils;

public class TankGame extends Table{
	
	//RIGHT NOW ON THE SCREEN WE ARE DRAWING 5 THINGS: BUILDINGS, PLAYERTANK, ENEMYTANKS, ENEMY BULLETS, PLAYER BULLETS AND BACKGROUNDS
	//THEY SHOULD ALL HAVE THEIR OWN OBJ FOR COLLISION DETECTION PURPOSES I THINK
	
	
	private BackGround backgroundRoad;
	
	public static PlayerTank playerTank;
	public static EnemyTanks enemyTanksObj;
	public static Weapons weaponsObj;
	public static EnemyWeapons enemyWeaponsObj;
	public static Buildings buildingsObj;
	
	
	private long lastBuildingTime = 0;
	private long lastTankTime = 0;
	private long previousTime = 0;
	private long prevTime = 0;
	
	public static int enemyShot;

	
	public Touchpad touchPad;
	public static TextButton fireButton;
	
	public boolean isButtonDown;
	
	
	public Array<Buildings> buildings;
	public Array<Weapons> weapons;
	public Array<EnemyTanks> enemyTanks;
	public static Array<EnemyWeapons> enemyWeapons;

	
	public TankGame(EnemyWeapons e){
		System.err.println("TankGame constructor is being called");
		addActor(e);

	}
	
	public TankGame() {		
		setBounds(0, 0, 800, 480);
		setClip(true);
		
		backgroundRoad = new BackGround(getWidth(),getHeight());

		addActor(backgroundRoad);
		
		playerTank = new PlayerTank(this);
		addActor(playerTank);
		
		buildings = new Array<Buildings>();	
		
		MyTouchPad myTPad= new MyTouchPad(this); 
		  touchPad = myTPad.getTouchPad();
		  addActor(touchPad);  
		  
		//Create the fire buttons using the styles
		fireButton = new TextButton("Fire", Styles.getFBStyle());//Changed textButtonStyle to skin ("Play, skin, "default")
		fireButton.pad(2, 32, 4, 32);
		fireButton.setBounds(700, 30, 64, 64);
			
		fireButton.addListener(new InputListener(){
			public boolean touchDown(InputEvent event, float x, float y, int pointer, int button){
				isButtonDown = true;
				return true;

			}
			public void touchUp(InputEvent event, float x, float y, int pointer, int button){
				isButtonDown = false;
			}
			
		});
		
		addActor(fireButton);
		
		weapons = new Array<Weapons>();			
		enemyTanks = new Array<EnemyTanks>();		
		enemyWeapons = new Array<EnemyWeapons>();
		
	}
		
	//the iterators are to remove objects from screen once out of bounds
	@Override
	public void act(float delta) {
		super.act(delta);		

		if(isButtonDown)
		{
			long currentTime = TimeUtils.millis();
			if (currentTime - previousTime > 100f)//.5 seconds
			{				
				spawnBullet();
				System.err.println("Fire");
				previousTime = currentTime;
			}
		}
		
		//SPAWN BUILDINGS, ENEMYTANKS, AND BULLETS HERE
		
		//how fast the buildings are spawning, NOT how fast they are moving
		if (TimeUtils.millis() - lastBuildingTime > 1000f) spawnBuilding();
		
		//enemy tank spawning
		float delay = MathUtils.random(1000f, 2000f);
		if (TimeUtils.millis() - lastTankTime > delay) spawnEnemyTank();
		
		//ENEMY BULLET SPAWNING		
		spawnEnemyBullet();
		
		
    //-------------------------------->ITERATORS<--------------------------------
		
		
		//building bounds checking
		Iterator<Buildings> iter = buildings.iterator();
		while (iter.hasNext()) {
			Buildings building = iter.next();
			if (building.getBounds().x + building.getWidth() <= 0) {
				iter.remove();
				removeActor(building);
			}		
		}
		
		//player bullet bounds checking
		Iterator<Weapons> iter2 = weapons.iterator();
		while (iter2.hasNext()) {
			weaponsObj = iter2.next();
			if (weaponsObj.getBounds().y  >= 480) {
				iter2.remove();
				removeActor(weaponsObj);
			}
		}
	
		//bounds checking
		Iterator<EnemyTanks>  iter3 = enemyTanks.iterator();
		while(iter3.hasNext()){
			enemyTanksObj = iter3.next();
			if (enemyTanksObj.getBounds().x + enemyTanksObj.getWidth() <= 0) {
				iter3.remove();
				removeActor(enemyTanksObj);
			}				
		}
				
		Iterator<EnemyWeapons>  iter4 = enemyWeapons.iterator();
		while(iter4.hasNext()){
			enemyWeaponsObj = iter4.next();
			if (enemyWeaponsObj.getBounds().y >= 480) {
				iter4.remove();
				removeActor(enemyWeaponsObj);
			}
		}			
	}
	
	
	
// ------------------>methods start here<------------------------------------
	
	
	
	
	private void spawnBuilding() {
		float yPos = 0;
		yPos = MathUtils.random(220,290);
		buildingsObj = new Buildings(getWidth(), yPos);
		buildings.add(buildingsObj);
		addActor(buildingsObj);
		lastBuildingTime = TimeUtils.millis();
	}

	private void spawnEnemyTank() {
		float yPos = 0;
		yPos = MathUtils.random(350,450);	
		enemyTanksObj = new EnemyTanks(getWidth(), yPos);
		enemyTanks.add(enemyTanksObj);
		addActor(enemyTanksObj);
		lastTankTime = TimeUtils.millis();
	}
	
	public void spawnBullet(){
		float xPos = 0;
		weaponsObj = new Weapons(xPos, getHeight());
		weapons.add(weaponsObj);
		addActor(weaponsObj);	
	}


	private void spawnEnemyBullet() {
		long thisTime = TimeUtils.millis();
		if (thisTime - prevTime > 500f)//.5 seconds
		{
			//System.err.println("spawnEnemyBullet() is being called");
			enemyWeaponsObj = new EnemyWeapons(enemyTanksObj);
			enemyWeapons.add(enemyWeaponsObj);
			addActor(enemyWeaponsObj);
			prevTime = thisTime;
		}
	}
	
	@Override
	public void draw(SpriteBatch batch, float parentAlpha) {
		batch.setColor(Color.WHITE);
		super.draw(batch, parentAlpha);
	}
}
