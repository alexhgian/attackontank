package com.aa.attackontank.screens;

import com.aa.attackontank.MyAttackOnTank;
import com.aa.attackontank.TankGame;
import com.badlogic.gdx.Gdx;
import com.badlogic.gdx.Screen;
import com.badlogic.gdx.graphics.GL10;
import com.badlogic.gdx.graphics.OrthographicCamera;
import com.badlogic.gdx.scenes.scene2d.Stage;

public class GameScreen implements Screen {
	private Stage stage;
	private TankGame tankGame;
	private OrthographicCamera camera;
	

	public GameScreen() {
   		camera = new OrthographicCamera();
   		camera.setToOrtho(false, 800, 480);
		stage = new Stage();

		
		tankGame = new TankGame();
		stage.addActor(tankGame);

	}


	@Override
	public void render(float delta) {
		Gdx.gl.glClearColor(0, 0, 0, 1);
		Gdx.gl.glClear(GL10.GL_COLOR_BUFFER_BIT);
		

        tankGame.playerTank.moveTank(tankGame.touchPad.getKnobPercentX(), tankGame.touchPad.getKnobPercentY());
        
        
		stage.act(delta);
		stage.draw();
	}

	@Override
	public void resize(int width, int height) {
		
		stage.setViewport(MyAttackOnTank.WIDTH, MyAttackOnTank.HEIGHT, true);
		stage.getCamera().translate(-stage.getGutterWidth(), -stage.getGutterHeight(), 0);
		
	}

	@Override
	public void show() {
		Gdx.input.setInputProcessor(stage);//Enables button press
		

	}

	@Override
	public void hide() {
		Gdx.input.setInputProcessor(null);
		
	}

	@Override
	public void pause() {
		// TODO Auto-generated method stub
		
	}

	@Override
	public void resume() {
		// TODO Auto-generated method stub
		
	}

	@Override
	public void dispose() {
		// TODO Auto-generated method stub
		
	}

	/*@Override
	public boolean touchDown(float x, float y, int pointer, int button) {
		// TODO Auto-generated method stub
		return false;
	}

	@Override
	public boolean tap(float x, float y, int count, int button) {
		// TODO Auto-generated method stub
		return false;
	}

	@Override
	public boolean longPress(float x, float y) {
		// TODO Auto-generated method stub
		return false;
	}

	@Override
	public boolean fling(float velocityX, float velocityY, int button) {
		if (velocityY < -100) tankGame.playerTank.moveUp();
		if (velocityY > 100) tankGame.playerTank.moveDown();
		return false;
	}

	@Override
	public boolean pan(float x, float y, float deltaX, float deltaY) {
		// TODO Auto-generated method stub
		return false;
	}

	@Override
	public boolean zoom(float initialDistance, float distance) {
		// TODO Auto-generated method stub
		return false;
	}

	@Override
	public boolean pinch(Vector2 initialPointer1, Vector2 initialPointer2,
			Vector2 pointer1, Vector2 pointer2) {
		// TODO Auto-generated method stub
		return false;
	}*/
    
}