package com.aa.attackontank.screens;

import static com.badlogic.gdx.scenes.scene2d.actions.Actions.forever;
import static com.badlogic.gdx.scenes.scene2d.actions.Actions.moveTo;
import static com.badlogic.gdx.scenes.scene2d.actions.Actions.sequence;

import com.aa.attackontank.Assets;
import com.badlogic.gdx.graphics.g2d.SpriteBatch;
import com.badlogic.gdx.scenes.scene2d.Actor;

public class BackGround extends Actor{
	
	public BackGround(float width, float height) {
		
		setWidth(width);
		setHeight(height);
		setPosition(width, 0);
		//addAction(forever(sequence(moveTo(0, 0, 1f), moveTo(width, 0))));
		addAction(forever(sequence(moveTo(0, 0, 2f), moveTo(width-160, 0))));
	}
	
	
	@Override
	public void draw(SpriteBatch batch, float parentAlpha) {
		super.draw(batch, parentAlpha);
		batch.draw(Assets.backGround, getX()-getWidth(), getY(), getWidth() * 2, getHeight());
		
	}

}
