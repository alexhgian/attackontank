package com.aa.attackontank.screens;

import com.aa.attackontank.MyAttackOnTank;
import com.aa.attackontank.ui.Styles;
import com.badlogic.gdx.Game;
import com.badlogic.gdx.Gdx;
import com.badlogic.gdx.Screen;
import com.badlogic.gdx.graphics.Color;
import com.badlogic.gdx.graphics.GL10;
import com.badlogic.gdx.graphics.OrthographicCamera;
import com.badlogic.gdx.graphics.g2d.BitmapFont;
import com.badlogic.gdx.graphics.g2d.TextureAtlas;
import com.badlogic.gdx.scenes.scene2d.InputEvent;
import com.badlogic.gdx.scenes.scene2d.Stage;
import com.badlogic.gdx.scenes.scene2d.ui.Label;
import com.badlogic.gdx.scenes.scene2d.ui.Label.LabelStyle;
import com.badlogic.gdx.scenes.scene2d.ui.Skin;
import com.badlogic.gdx.scenes.scene2d.ui.Table;
import com.badlogic.gdx.scenes.scene2d.ui.TextButton;
import com.badlogic.gdx.scenes.scene2d.utils.ClickListener;

public class MainMenuScreen implements Screen {
	
    //public MainMenuScreen(final Drop gam)....        
    final MyAttackOnTank game;
    
    OrthographicCamera camera;
    
    private TextureAtlas atlas;
    private Stage stage;
    private Skin skin;
    private Table table;
    private TextButton buttonPlay,buttonSetting,buttonExit;
    private BitmapFont white,black;
    private Label heading;
    
    
   	public MainMenuScreen(final MyAttackOnTank gam) {
   		game = gam;
    
   		camera = new OrthographicCamera();
   		camera.setToOrtho(false, 800, 480);
    
   	}
	@Override
    public void render(float delta) {
        Gdx.gl.glClearColor(0, 0, 0, 1);
        Gdx.gl.glClear(GL10.GL_COLOR_BUFFER_BIT);
        
        Table.drawDebug(stage);
        
		stage.act(delta);
		stage.draw();      
    }

	@Override
	public void resize(int width, int height) {
		System.err.println("MainMenuScreen.java Resize - Width: " + width + ", Height: " + height);
		// TODO Auto-generated method stub
		stage.setViewport(width, height, true);
		table.setClip(true);
		table.setSize(width,height);
	}

	@Override
	public void show() {
		stage = new Stage();
		
		Gdx.input.setInputProcessor(stage);//Enables button press
		
		atlas = new TextureAtlas("ui/uiskin.atlas");
		//skin = new Skin(Gdx.files.internal("ui/menuSkin.json"),atlas);
		skin = new Skin(atlas);
		
		table = new Table();
		table.setBounds(0,0,Gdx.graphics.getWidth(),Gdx.graphics.getHeight());
		
		//Create the button style
		/*TextButtonStyle textButtonStyle = new TextButtonStyle();
		textButtonStyle.up = skin.getDrawable("default-round");
		textButtonStyle.down = skin.getDrawable("default-round-down");
		textButtonStyle.pressedOffsetX = 1;
		textButtonStyle.pressedOffsetY = -1;	

		textButtonStyle.font = white;*/
		white = new BitmapFont();
		white.scale(2);
		//Create the actual buttons using the styles
		buttonPlay = new TextButton("Play", Styles.getMenuStyle());//Changed textButtonStyle to skin ("Play, skin, "default")
		buttonPlay.pad(2, 32, 4, 32);
		buttonPlay.addListener(new ClickListener(){
			public void clicked(InputEvent event, float x, float y){
				( (Game) Gdx.app.getApplicationListener() ).setScreen(new GameScreen());
			}			
		});
		
		
		//Create another button
		buttonSetting = new TextButton("Setting", Styles.getMenuStyle());
		buttonSetting.pad(2, 24, 4, 24);
		
		//Create another button
		buttonExit = new TextButton("Exit", Styles.getMenuStyle());
		buttonExit.pad(2, 34, 4, 34);
		buttonExit.addListener(new ClickListener(){
			public void clicked(InputEvent event, float x, float y){
				Gdx.app.exit();
			}
		});
		
		//Create label for title
		LabelStyle headingStyle = new LabelStyle(white, Color.WHITE);		
		heading = new Label(MyAttackOnTank.TITLE, headingStyle);//removed headingStyle
		heading.setFontScale(1);
		
		//Add all the stuff we created
		table.add(heading);
		table.getCell(heading).spaceBottom(2);
		table.row();
		table.add(buttonPlay);//Add button to table
		table.row();
		table.add(buttonSetting);//Add button to table
		table.row();
		table.add(buttonExit);//Add button to table
		stage.addActor(table);//Add table to stage
	}

	@Override
	public void hide() {
		// TODO Auto-generated method stub
		
	}

	@Override
	public void pause() {
		// TODO Auto-generated method stub
		
	}

	@Override
	public void resume() {
		// TODO Auto-generated method stub
		
	}

	@Override
	public void dispose() {
		// TODO Auto-generated method stub
		stage.dispose();
		skin.dispose();
		atlas.dispose();
		white.dispose();
		black.dispose();
		
	}

    //Rest of class still omitted...

}