package com.aa.attackontank;

import static com.badlogic.gdx.scenes.scene2d.actions.Actions.moveTo;

import com.badlogic.gdx.graphics.g2d.SpriteBatch;
import com.badlogic.gdx.math.Rectangle;
import com.badlogic.gdx.scenes.scene2d.Actor;

public class Weapons extends Actor{
	
	
	private Rectangle bounds = new Rectangle();
	
	public Weapons(float x, float y) {
		setWidth(32);
		setHeight(32);
		//where it spawns
		setPosition(TankGame.playerTank.getX()+16, TankGame.playerTank.getY()+ 70);

		System.err.println("W: "+ getX() + " H: " + y);
		//spawning speed
		addAction(moveTo(getX(), y, 1));
	}
	
	
	
	@Override
	public void act(float delta){
		super.act(delta);
		updateBounds();
	}
	
	private void updateBounds() {
		bounds.set(getX(), getY(), getWidth(), getHeight());
	}
	
	public Rectangle getBounds() {
		return bounds;
	}
	
	
	@Override
	public void draw(SpriteBatch batch, float parentAlpha) {
		batch.setColor(getColor().r, getColor().g, getColor().b, getColor().a);	
		//direction it is spawning
		batch.draw(Assets.bullet, getX(), getY(), getWidth()/2, getHeight()/2, getWidth(), getHeight(), 1, 1, getRotation());
	}

}
