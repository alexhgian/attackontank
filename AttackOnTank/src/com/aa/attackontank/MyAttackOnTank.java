package com.aa.attackontank;

import com.aa.attackontank.screens.MainMenuScreen;
import com.badlogic.gdx.Game;
import com.badlogic.gdx.graphics.g2d.BitmapFont;
import com.badlogic.gdx.graphics.g2d.SpriteBatch;

public class MyAttackOnTank extends Game {
	public final static String TITLE = "Attack On Tank";
    public static SpriteBatch batch;
    public BitmapFont font;
    
    public final static int WIDTH = 800;
	public final static int HEIGHT = 480;

    public void create() {
    		
    		Assets.load();
            batch = new SpriteBatch();
            //Use LibGDX's default Arial font.
            font = new BitmapFont();
            this.setScreen(new MainMenuScreen(this));
    }

    public void render() {
            super.render(); //important!
    }
    
    public void dispose() {
            batch.dispose();
            font.dispose();
    }
 }

