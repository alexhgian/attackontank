package com.aa.attackontank;

import static com.badlogic.gdx.scenes.scene2d.actions.Actions.fadeOut;
import static com.badlogic.gdx.scenes.scene2d.actions.Actions.moveTo;
import static com.badlogic.gdx.scenes.scene2d.actions.Actions.removeActor;

import com.badlogic.gdx.graphics.Color;
import com.badlogic.gdx.graphics.g2d.SpriteBatch;
import com.badlogic.gdx.math.MathUtils;
import com.badlogic.gdx.math.Rectangle;
import com.badlogic.gdx.scenes.scene2d.Actor;
import com.badlogic.gdx.utils.Array;
import com.badlogic.gdx.utils.TimeUtils;

public class EnemyTanks extends Actor{
	
	Array<Rectangle> enemyTanks;
	
	long prevTime =0;
	
private Rectangle bounds = new Rectangle();
	
	public EnemyTanks(float x, float y) {
		setWidth(50);
		setHeight(50);
		setPosition(0, y - getHeight()/2);
		//spawning speed
		
		int color = MathUtils.random(0, 3);
		if (color == 0) setColor(Color.ORANGE);
		if (color == 1) setColor(Color.GREEN);
		if (color == 2) setColor(Color.PINK);
		if (color == 3) setColor(Color.WHITE);
		
		addAction(moveTo(800, getY(), 5));
		//the third parameter was previously  MathUtils.random(4.0f, 6.0f)
	}
	
	@Override
	public void act(float delta){
		super.act(delta);
		updateBounds();
	}
	
	private void updateBounds() {
		bounds.set(getX(), getY(), getWidth(), getHeight());
	}
	

	public Rectangle getBounds() {
		return bounds;
	}
	
	public void crash() {
		clearActions();
		addAction(fadeOut(1f));
		removeActor();
	}

	@Override
	public void draw(SpriteBatch batch, float parentAlpha) {
		batch.setColor(getColor().r, getColor().g, getColor().b, getColor().a);		
		batch.draw(Assets.enemyTank, getX(), getY(), getWidth()/2, getHeight()/2, getWidth(), getHeight(), 1, 1, getRotation());
	}

	
}
