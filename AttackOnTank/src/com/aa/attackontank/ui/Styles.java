package com.aa.attackontank.ui;

import com.badlogic.gdx.Gdx;
import com.badlogic.gdx.graphics.g2d.BitmapFont;
import com.badlogic.gdx.graphics.g2d.TextureAtlas;
import com.badlogic.gdx.scenes.scene2d.ui.Skin;
import com.badlogic.gdx.scenes.scene2d.ui.TextButton.TextButtonStyle;

public class Styles {
	private static Skin fbSkin,uiSkin;
	private static BitmapFont white;
	
	//Fire button style
	public static TextButtonStyle getFBStyle()
	{		
		TextureAtlas atlasFB = new TextureAtlas(Gdx.files.internal("ui/FireButtons.txt"));
		fbSkin = new Skin(atlasFB);
		//Create the button style
		TextButtonStyle textButtonStyle = new TextButtonStyle();
		textButtonStyle.up = fbSkin.getDrawable("FireButton");
		textButtonStyle.down = fbSkin.getDrawable("FireButton2");
		textButtonStyle.pressedOffsetX = 4;
		textButtonStyle.pressedOffsetY = -4;
		
		white = new BitmapFont();
		white.scale(2);
		textButtonStyle.font = white;
		
		return textButtonStyle;
	}
	
	
	
	public static TextButtonStyle getMenuStyle()
	{
		TextureAtlas atlasUI = new TextureAtlas(Gdx.files.internal("ui/uiskin.atlas"));
		uiSkin = new Skin(atlasUI);
		//Create the button style
		TextButtonStyle textButtonStyle = new TextButtonStyle();
		textButtonStyle.up = uiSkin.getDrawable("default-round");
		textButtonStyle.down = uiSkin.getDrawable("default-round-down");
		textButtonStyle.pressedOffsetX = 1;
		textButtonStyle.pressedOffsetY = -1;	
		
		white = new BitmapFont();
		white.scale(2);
		textButtonStyle.font = white;	
		return textButtonStyle;
	}
}
