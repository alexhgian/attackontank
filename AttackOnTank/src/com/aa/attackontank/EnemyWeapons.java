package com.aa.attackontank;

import static com.badlogic.gdx.scenes.scene2d.actions.Actions.moveTo;

import com.badlogic.gdx.graphics.Color;
import com.badlogic.gdx.graphics.g2d.SpriteBatch;
import com.badlogic.gdx.math.MathUtils;
import com.badlogic.gdx.math.Rectangle;
import com.badlogic.gdx.scenes.scene2d.Actor;

public class EnemyWeapons extends Actor{
	
private Rectangle bounds = new Rectangle();
	
	public EnemyWeapons(EnemyTanks myTank ) {
		setWidth(28);
		setHeight(28);
		//where it spawns
		setPosition(myTank.getX(), myTank.getY()-50);

		//spawning speed
		addAction(moveTo(getX(), -200, 3));
	}
	

	@Override
	public void act(float delta){
		super.act(delta);
		updateBounds();
	}
	
	private void updateBounds() {
		bounds.set(getX(), getY(), getWidth(), getHeight());
	}
	
	public Rectangle getBounds() {
		return bounds;
	}
	
	@Override
	public void draw(SpriteBatch batch, float parentAlpha) {
		batch.setColor(getColor().r, getColor().g, getColor().b, getColor().a);	
		//direction it is spawning
		batch.draw(Assets.enemyBullet, getX(), getY(), getWidth()/2, getHeight()/2, getWidth(), getHeight(), 1, 1, getRotation());

	}

}

